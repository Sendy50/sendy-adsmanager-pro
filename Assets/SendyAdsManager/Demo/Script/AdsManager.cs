﻿using System;
using System.Collections;
using System.Collections.Generic;
using SendyAdsManager;
using UnityEngine;


public class AdsManager : MonoBehaviour
{
    private static Action<bool> _rewardCallback;
    private static Action<bool> _interstitialcallback;
    public GameObject NativeDialog;


    private void Start()
    {
       
    }

    public void ShowMoreApp()
    {
        Advertising.ShowMoreapp(true,Color.white);
    }
    
    public void ShowNativeDemoDialog(bool show)
    {
        NativeDialog.SetActive(show);
        if(!show)
            Advertising.RequestNativeAd(); 
    }
    
    
  
    public void ShowStartMoreApp()
    {
        Advertising.ShowStartMoreappPage(b =>
        {
            
            //write here your code after close moreapp screen
            Debug.LogError("Close Start More App Screen");
        } );
    }

    //Banner Methods
    public static void ShowBannerAds()
    {
        Advertising.ShowBannerAd();
    }

    public static void HideBannerAds()
    {
        Advertising.HideCustomBannerAd();
    }
    
    public static void DestroyBannerAds()
    {
        Advertising.DestroyCustomBannerAd();
    }


    //interstitial Methods
    public static void ShowInterstitialAds()
    {
        Advertising.ShowInterstitialAd((adNetwork, adComplete) =>
            {
                if (adComplete)
                {
                    Debug.LogError("Interstitial Ads Complete : adNetwork : "+adNetwork);
                }
                else
                {
                    Debug.LogError("Interstitial Ads Show Fail : adNetwork : "+adNetwork);
                }
            });
    }

    
    //Reward Methods
    public static void ShowRewardAds()
    {
        Advertising.ShowRewardedAd((adNetwork, adComplete) =>
        {
            if (adComplete)
            {
                Debug.LogError("Reward Ads Complete : adNetwork : "+adNetwork);
            }
            else
            {
                Debug.LogError("Reward Ads Skipped : adNetwork : "+adNetwork);
            }
        });
    }
}
