﻿using System;
using System.Collections.Generic;
using UnityEditor;
using SendyAdsManager.Internal;

namespace SendyAdsManager.Editor
{
    [CustomPropertyDrawer(typeof(StringStringSerializableDictionary))]
    [CustomPropertyDrawer(typeof(StringAdIdSerializableDictionary))]
    public class AnySerializableDictionaryPropertyDrawer : SerializableDictionaryPropertyDrawer
    {

    }

    public class AnySerializableDictionaryStoragePropertyDrawer : SerializableDictionaryStoragePropertyDrawer
    {

    }
}
