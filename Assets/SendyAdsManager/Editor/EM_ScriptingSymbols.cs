﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace SendyAdsManager.Editor
{
    public static class EM_ScriptingSymbols
    {
        // "SENDY_ADMANAGER"
        // "Sendy_ADMANAGER_PRO"
        public const string EasyMobile = "SENDY_ADMANAGER;SENDY_ADMANAGER_PRO";
        // public const string EasyMobile = "EASY_MOBILE;SENDY_ADMANAGER_PRO";
        // public const string UnityIAP = "EM_UIAP";
        // public const string GooglePlayGames = "EM_GPGS";
        // public const string NoGooglePlayGames = "NO_GPGS";
        // public const string OneSignal = "EM_ONESIGNAL";
        // public const string FirebaseMessaging = "EM_FIR_MESSAGING";
        // public const string ContactsSubmodule = "EM_CONTACTS";
        // public const string CameraGallerySubmodule = "EM_CAMERA_GALLERY";
        // public const string UniversalRenderPipeline = "EM_URP";

        // Ad networks
        public const string AdColony = "SA_ADCOLONY";
        public const string AdMob = "SA_ADMOB";
        public const string AppLovin = "SA_APPLOVIN";
        public const string Chartboost = "SA_CHARTBOOST";
        public const string FairBid = "SA_FAIRBID";
        public const string MoPub = "SA_MOPUB";
        public const string FBAudience = "SA_FBAN";
        public const string IronSource = "SA_IRONSOURCE";
        public const string TapJoy = "SA_TAPJOY";
        public const string UnityAds = "SA_UNITY_ADS";
        public const string Vungle = "SA_VUNGLE";
        public const string UnityMonetization = "UNITY_MONETIZATION";
        public const string Firebase = "SA_FIREBASE";

        public static string[] GetAllSymbols()
        {
            return EM_EditorUtil.GetConstants(typeof(EM_ScriptingSymbols)).Select(c => c.GetRawConstantValue() as string).ToArray();
        }

    }
}

