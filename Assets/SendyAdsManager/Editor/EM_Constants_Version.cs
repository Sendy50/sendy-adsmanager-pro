﻿using UnityEngine;
using System.Collections;

namespace SendyAdsManager.Editor
{
    public static partial class EM_Constants
    {
        // Product variant
        public const string ProductVariant = "Pro";

        // Current version
        public const string versionString = "1.0.0";
        public const int versionInt = 0x021300;
    }
}

