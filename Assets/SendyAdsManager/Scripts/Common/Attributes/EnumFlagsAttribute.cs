﻿using UnityEngine;
using System;

namespace SendyAdsManager.Internal
{
    public class EnumFlagsAttribute : PropertyAttribute
    {
        public string enumName;

        public EnumFlagsAttribute()
        {
        }

        public EnumFlagsAttribute(string name)
        {
            enumName = name;
        }
    }
}