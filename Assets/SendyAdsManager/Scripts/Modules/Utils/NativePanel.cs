﻿using System;
using System.Collections;
using System.Collections.Generic;
#if SA_ADMOB
using GoogleMobileAds.Api;
#endif
using UnityEngine;
using UnityEngine.UI;


namespace SendyAdsManager
{
    public class NativePanel : MonoBehaviour
    {
#if SA_ADMOB
        private UnifiedNativeAd nativeAd;
#endif
        public const string TemplateId = "10085730";

        public GameObject NativePanell;
        public RawImage AdsIcon;
        public RawImage Banner;
        public RawImage AdsChoiceIcon;
        public Text Adstext;
        public Text Adsverttext;
        public Text Btntext;
        public Text Bodytext;

        // Start is called before the first frame update
        IEnumerator Start()
        {
            Debug.LogError("@Admob Wait For Native Ad Load");
#if SA_ADMOB
            yield return new WaitUntil(() => Advertising.nativeAd != null);
            if (Advertising.IsNativeLoaded())
            {
                nativeAd = Advertising.nativeAd;
                NativePanell.SetActive(true);

                Debug.LogError("@Admob Native Loaded");

                AdsIcon.texture = this.nativeAd.GetIconTexture();
                Banner.texture = this.nativeAd.GetImageTextures()[0];
                AdsChoiceIcon.texture = this.nativeAd.GetAdChoicesLogoTexture();
                Adstext.text = this.nativeAd.GetHeadlineText();
                Adsverttext.text = this.nativeAd.GetAdvertiserText();
                Btntext.text = this.nativeAd.GetCallToActionText();
                Bodytext.text = this.nativeAd.GetBodyText();

                List<GameObject> ImageList = new List<GameObject>();
                ImageList.Add(Banner.gameObject);

                nativeAd.RegisterIconImageGameObject(AdsIcon.gameObject);
                nativeAd.RegisterImageGameObjects(ImageList);
                nativeAd.RegisterAdChoicesLogoGameObject(AdsChoiceIcon.gameObject);
                nativeAd.RegisterHeadlineTextGameObject(Adstext.gameObject);
                nativeAd.RegisterAdvertiserTextGameObject(Adsverttext.gameObject);
                nativeAd.RegisterCallToActionGameObject(Btntext.gameObject);
                nativeAd.RegisterBodyTextGameObject(Bodytext.gameObject);
            }

#endif
            yield return null;
        }


        public void OnPlayBtnClick()
        {
            Advertising.ShowStartMoreappPage(Advertising.ExitCallBack);
            Advertising.RequestNativeAd();
        }
    }
}