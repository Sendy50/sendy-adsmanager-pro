﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdDialog : MonoBehaviour
{
    public GameObject Loader;
    private float rotationsPerMinute = 60.0f;
    void Update()
    {
        // Debug.LogError("Dialog Loading .. ");
        // Loader.transform.Rotate(0,6.0);
        Loader.transform.Rotate(0,0,-6.0f*rotationsPerMinute*Time. deltaTime);
    }
}
