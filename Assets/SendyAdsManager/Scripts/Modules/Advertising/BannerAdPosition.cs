﻿using System;

namespace SendyAdsManager
{
    public enum BannerAdPosition
    {
        Top,
        Bottom,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}

