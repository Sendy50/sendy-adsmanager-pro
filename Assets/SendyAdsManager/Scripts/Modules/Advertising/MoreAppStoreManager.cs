﻿using System.Collections;
using System.Collections.Generic;
using SendyAdsManager;
using UnityEngine;
using UnityEngine.UI;

public class MoreAppStoreManager : MonoBehaviour
{
    private List<AppsListItem> appListItems;
    private List<AppsListItem> appListItems2;
    public GameObject _appItemPrefab;
    public GameObject _horizontalAppView;
    public GameObject _gridAppView;
    private GameObject dialog;

    // Start is called before the first frame update
    void Start()
    {
        ShowDialog(true);
        appListItems = AdsUtils.getApps();
        appListItems2 = AdsUtils.getApps2();
        Debug.LogError("@@ AppStore List size : " + appListItems.Count);
        Debug.LogError("@@ AppStore List2 size : " + appListItems2.Count);
        if (appListItems.Count > 0)
            SetUpAppsUi();
        else
        {
            ShowDialog(false);
            Debug.LogError("@@ AppStore List Empty  ");
        }
    }

    public void ShowDialog(bool show)
    {
        if (show)
        {
            dialog = Instantiate(Resources.Load("DialogCanvas") as GameObject);
            dialog.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text =
                "APP LOADING";
        }
        else
        {
            if (dialog != null)
                DestroyImmediate(dialog);
        }
    }

    private void SetUpAppsUi()
    {
        // float width = GetWidth();
        // float height = GetHeight();
        //
        // Debug.LogError("Set Ui Width : "+width+" height : "+height);

        for (int i = 0; i < appListItems.Count; i++)
        {
            GameObject appItem1 = Instantiate(_appItemPrefab);
            appItem1.transform.parent = _horizontalAppView.transform;
            appItem1.GetComponent<AppItem>().SetUi(appListItems[i]);
            appItem1.transform.localScale = Vector3.one;
        }

        for (int i = 0; i < appListItems2.Count; i++)
        {
            GameObject appItem2 = Instantiate(_appItemPrefab);
            appItem2.transform.parent = _gridAppView.transform;
            appItem2.GetComponent<AppItem>().SetUi(appListItems2[i]);
            appItem2.transform.localScale = Vector3.one;
        }

        transform.GetComponent<Canvas>().enabled = true;
        ShowDialog(false);
    }

   
    public void ExitApp(bool exit)
    {
        if (exit)
        {
            Debug.LogError("Dialog And Game Exit");
            Advertising.ShowMoreapp(false, Color.white);
            Application.Quit();
        }
        else
        {
            Debug.LogError("Dialog Exit");
            Advertising.ShowMoreapp(false, Color.white);
        }
    }

    public void SetTheme(Color bgColor)
    {
        //_appItemPrefab.GetComponent<>()
    }
}