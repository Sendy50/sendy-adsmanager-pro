﻿using UnityEngine;
using System.Collections;

namespace SendyAdsManager
{
    public enum AdOrientation
    {
        AdOrientationPortrait,
        AdOrientationLandscape,
        AdOrientationAll
    }
}
