﻿using UnityEngine;
using System.Collections;

namespace SendyAdsManager
{
    public enum AdType
    {
        Banner,
        Interstitial,
        Rewarded
    }
}
