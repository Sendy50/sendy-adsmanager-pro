﻿using UnityEngine;
using System.Collections;

namespace SendyAdsManager
{
    public enum AdChildDirectedTreatment
    {
        Unspecified,
        Yes,
        No
    }
}
