﻿using UnityEngine;
using System.Collections;

namespace SendyAdsManager
{
    // List of all supported ad networks
    public enum AdNetwork
    {
        None,
        AdColony,
        AdMob,
        AppLovin,
        AudienceNetwork,
        Chartboost,
        FairBid,
        IronSource,
        MoPub,
        TapJoy,
        UnityAds,
        Vungle,
    }

    public enum BannerAdNetwork
    {
        None = AdNetwork.None,
        AdColony = AdNetwork.AdColony,
        AdMob = AdNetwork.AdMob,
        AppLovin = AdNetwork.AppLovin,
        AudienceNetwork = AdNetwork.AudienceNetwork,
        FairBid = AdNetwork.FairBid,
        IronSource = AdNetwork.IronSource,
        MoPub = AdNetwork.MoPub,
        UnityAds = AdNetwork.UnityAds,
        Vungle = AdNetwork.Vungle,
    }

    public enum Percentage
    {
        Zero = 0,
        Ten = 10,
        Twenty = 20,
        Thirty = 30,
        Fourty = 40,
        Fifty = 50,
        Sixty = 60,
        Seventy = 70,
        Eight = 80,
        Ninety = 90,
        Hundred = 90
    }
   

    public enum InterstitialAdNetwork
    {
        None = AdNetwork.None,
        AdColony = AdNetwork.AdColony,
        AdMob = AdNetwork.AdMob,
        AppLovin = AdNetwork.AppLovin,
        AudienceNetwork = AdNetwork.AudienceNetwork,
        Chartboost = AdNetwork.Chartboost,
        FairBid = AdNetwork.FairBid,
        IronSource = AdNetwork.IronSource,
        MoPub = AdNetwork.MoPub,
        TapJoy = AdNetwork.TapJoy,
        UnityAds = AdNetwork.UnityAds,
        Vungle = AdNetwork.Vungle,
    }

    public enum RewardedAdNetwork
    {
        None = AdNetwork.None,
        AdColony = AdNetwork.AdColony,
        AdMob = AdNetwork.AdMob,
        AppLovin = AdNetwork.AppLovin,
        AudienceNetwork = AdNetwork.AudienceNetwork,
        Chartboost = AdNetwork.Chartboost,
        FairBid = AdNetwork.FairBid,
        IronSource = AdNetwork.IronSource,
        MoPub = AdNetwork.MoPub,
        TapJoy = AdNetwork.TapJoy,
        UnityAds = AdNetwork.UnityAds,
        Vungle = AdNetwork.Vungle,
    }
}