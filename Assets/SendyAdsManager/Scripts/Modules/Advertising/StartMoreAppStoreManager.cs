﻿using System.Collections;
using System.Collections.Generic;
using SendyAdsManager;
using UnityEngine;
using UnityEngine.UI;

public class StartMoreAppStoreManager : MonoBehaviour
{
    private List<AppsListItem> appListItems;
    public GameObject _appItemPrefab;
    public GameObject _gridAppView;
    private  GameObject dialog;
    
   

    // Start is called before the first frame update
    void Start()
    {
        ShowDialog(true);
        appListItems = AdsUtils.getApps2();
        Debug.LogError("@@ AppStore List size : "+appListItems.Count);
        if(appListItems.Count > 0)
            SetUpAppsUi();
        else
        {
            ShowDialog(false);
            Debug.LogError("@@ AppStore List Empty  ");
        }
    }


    public void HideStartMoreAppPanel()
    {
        Advertising.ShowStartMoreappPage(Advertising.ExitCallBack);
    }

    public void ShowDialog(bool show)
    {
        if (show)
        {
            dialog = Instantiate(Resources.Load("DialogCanvas") as GameObject);
            dialog.transform.GetChild(0).transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text =
                "APP LOADING";
            //Debug.LogError("Show Dialog..");
        }
        else
        {
            if(dialog!= null)
                DestroyImmediate(dialog);
        }
    }

    private void SetUpAppsUi()
    {

        for (int i = 0; i < appListItems.Count; i++)
        {
            GameObject appItem2 = Instantiate(_appItemPrefab);
            appItem2.transform.parent = _gridAppView.transform;
            appItem2.GetComponent<AppItem>().SetUi(appListItems[i]);
            appItem2.transform.localScale = Vector3.one;
        }
       
        transform.GetComponent<Canvas>().enabled = true;
        ShowDialog(false);
    }

    
    public void ExitApp(bool exit)
    {
        if (exit)
        {
            Debug.LogError("Dialog And Game Exit");
            Advertising.ShowMoreapp(false, Color.white);
            Application.Quit();
        }
        else
        {
            Debug.LogError("Dialog Exit");
            Advertising.ShowMoreapp(false,  Color.white);
        }    
    }

    public void SetTheme(Color bgColor)
    {
        //_appItemPrefab.GetComponent<>()
    }
    
}