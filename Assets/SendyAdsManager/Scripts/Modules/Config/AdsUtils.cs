﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using SendyAdsManager;
using UnityEngine;

public class AdsUtils
{
    private static String TAG = "AdsUtils";

    public static int getAdmobAdPercentages()
    {
        Response response = LoadFile();
        return int.Parse(response.getRemoteData().getAdmob().getPercentages());
    }

    public static bool getAdmobTestMode()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAdmob().getTestMode();
    }

    public static String getAdmobInterstitialAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAdmob().getInterstitialAd();
    }

    public static String getAdmobBannerAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAdmob().getBannerAd();
    }

    public static String getAdmobNativeAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAdmob().getNativeAd();
    }

    public static String getAdmobRewardAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAdmob().getRewardAd();
    }

    public static int getFacebookAdPercentages()
    {
        Response response = LoadFile();
        return int.Parse(response.getRemoteData().getFacebook().getPercentages());
    }

    public static bool getFbTestMode()
    {
        Response response = LoadFile();
        return response.getRemoteData().getFacebook().getTestMode();
    }

    public static String getFacebookInterstitialAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getFacebook().getInterstitialAd();
    }

    public static String getFacebookBannerAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getFacebook().getBannerAd();
    }

    public static String getFacebookNativeAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getFacebook().getNativeAd();
    }

    public static String getFacebookRewardAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getFacebook().getRewardAd();
    }

    public static int getUnityAdPercentages()
    {
        Response response = LoadFile();
        return int.Parse(response.getRemoteData().getUnity().getPercentages());
    }

    public static bool getUnityTestMode()
    {
        Response response = LoadFile();
        return response.getRemoteData().getUnity().getTestMode();
    }

    public static String getUnityInterstitialAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getUnity().getInterstitialAd();
    }

    public static String getUnityBannerAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getUnity().getBannerAd();
    }

    public static String getUnityNativeAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getUnity().getNativeAd();
    }

    public static String getUnityRewardAd()
    {
        Response response = LoadFile();
        return response.getRemoteData().getUnity().getRewardAd();
    }

    public static List<AdNetwork> getAdsTypeWithPercentages()
    {
        List<AdNetwork> adTypes = new List<AdNetwork>();
        int admobPer = getAdmobAdPercentages() / 10;
        int facebookPer = getFacebookAdPercentages() / 10;
        int unityPer = getUnityAdPercentages() / 10;
        int index = 1;
        int iSize = 10;
        for (int i = 0; i < iSize; i++)
        {
            if (index == 1 && admobPer > 0)
            {
                adTypes.Add(AdNetwork.AdMob);
                index += 1;
                admobPer -= 1;
            }
            else if (index == 2 && facebookPer > 0)
            {
                adTypes.Add(AdNetwork.AudienceNetwork);
                index += 1;
                facebookPer -= 1;
            }
            else if (index == 3 && unityPer > 0)
            {
                adTypes.Add(AdNetwork.UnityAds);
                index = 1;
                unityPer -= 1;
            }
            else
            {
                iSize += 1;
                if (admobPer > 0)
                {
                    index = 1;
                }
                else if (facebookPer > 0)
                {
                    index = 2;
                }
                else if (unityPer > 0)
                {
                    index = 3;
                }
            }
        } // Debug.LogError(TAG + " : adTypes.Count :" + adTypes.Count);

        return adTypes;
    }

    public static AdNetwork getCurrentAd()
    {
        // int index = tinyDB.getInt("current_index");
        int index = PlayerPrefs.GetInt("current_index", 0);
        Debug.LogError("Current_index : " + index);
        if (index > getAdsTypeWithPercentages().Count - 1)
        {
            index = 0;
        }

        AdNetwork adType = getAdsTypeWithPercentages()[index];
        PlayerPrefs.SetInt("current_index", index + 1);
        PlayerPrefs.Save();
        return adType;
    }

    public static int getAppAvailability()
    {
        Response response = LoadFile();
        return int.Parse(response.getRemoteData().getAppAvailability());
    }

    public static int getAppVersionCode()
    {
        Response response = LoadFile();
        return int.Parse(response.getRemoteData().getVersionCode());
    }

    public static List<AppsListItem> getApps()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAppsList();
    }

    public static List<AppsListItem> getApps2()
    {
        Response response = LoadFile();
        return response.getRemoteData().getAppsList2();
    }

    public static OptionalApp getOptionalApp()
    {
        Response response = LoadFile();
        return response.getRemoteData().getOptionalApp();
    }


    public static void SaveFile(Response response)
    {
        RemoteConfiguration.ResponseRef = response;
        Debug.LogError("Save File Called ");
        string destination = Application.persistentDataPath + "/responceData.json";
        FileStream file;
        if (File.Exists(destination))
            file = File.OpenWrite(destination);
        else
            file = File.Create(destination);
        BinaryFormatter bf = new BinaryFormatter();
        try
        {
            bf.Serialize(file, response);
        }
        catch (SerializationException e)
        {
            Console.WriteLine("Failed to serialize. Reason: " + e.Message);
            throw;
        }
        finally
        {
            file.Close();
        }

        Debug.LogError("SaveFile Success");
    }

    public static Response LoadFile()
    {
        if (RemoteConfiguration.ResponseRef == null)
        {
#if UNITY_EDITOR
            string destination = Path.Combine(Application.streamingAssetsPath, "responceData.json");
            // Debug.LogError(destination);
            return JsonUtility.FromJson<Response>(File.ReadAllText(destination));
#else
        string destination = Application.persistentDataPath + "/responceData.json";
        Debug.LogError(destination);
#endif
            FileStream file = null;
            if (File.Exists(destination))
                file = File.OpenRead(destination);
            else
            {
                Debug.LogError("@@ 1 File not found");
                RemoteConfiguration.ResponseRef = JsonUtility.FromJson<Response>(GetStreamingJson());
                SaveFile(RemoteConfiguration.ResponseRef);
                return RemoteConfiguration.ResponseRef;
            }

            BinaryFormatter bf = new BinaryFormatter();
            try
            {
                RemoteConfiguration.ResponseRef = (Response) bf.Deserialize(file);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
                throw;
            }
            finally
            {
                file.Close();
            }

            return RemoteConfiguration.ResponseRef;
        }

        return RemoteConfiguration.ResponseRef;
    }

    public static string GetStreamingJson()
    {
        string json = "";
        string path;
#if UNITY_ANDROID
        path = Application.streamingAssetsPath + "/responceData.json";
        WWW www = new WWW(path);
        while (!www.isDone)
        {
            Debug.LogError("Streaming Data Loading....");
        }

        json = www.text;
        Debug.LogError("Streaming Json : " + json);

#elif UNITY_IOS
        path = Application.streamingAssetsPath + "/responceDataIos.json";
        json = File.ReadAllText(path);
        Debug.LogError("Streaming Json : "+json);
#endif

        return json;
    }

    private static void initAppData()
    {
        //here Save Our json File
        string path;
#if UNITY_ANDROID
        path = Application.streamingAssetsPath + "/responceData.json";
        Debug.LogError("Fix Json Path : " + path);
        WWW www = new WWW(path);
        while (!www.isDone)
        {
            Debug.LogError("RemoteConfig StaticAdRemoteData Load Failed ...");
        }

        string json = www.text;
        Debug.LogError("RemoteConfig StaticAdRemoteData Loaded : json " + json);
#elif UNITY_IOS
                path = Application.streamingAssetsPath + "/responceDataIos.json";
                json = File.ReadAllText(path);
                Debug.LogError("Json IOS  : "+json);
#endif
        Response response = JsonUtility.FromJson<Response>(json);
        AdsUtils.SaveFile(response);
    }
}