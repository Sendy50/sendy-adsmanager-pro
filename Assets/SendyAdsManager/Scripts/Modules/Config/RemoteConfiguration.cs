﻿
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

#if SA_FIREBASE
using Firebase.Extensions;
#endif

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using SendyAdsManager;
using SendyAdsManager.Internal;
using UnityEditor;

public class RemoteConfiguration : MonoBehaviour
{

    public static Response ResponseRef;
    
#if SA_FIREBASE
    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
#endif
    
    protected bool isFirebaseInitialized = false;


    private void Awake()
    {
#if SA_FIREBASE
        Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
#endif
    }

    protected virtual void Start() {
#if SA_FIREBASE
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available) {
                InitializeFirebase();
            } else {
                Debug.LogError(
                    "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
#endif
    }
    
#if SA_FIREBASE 
    void InitializeFirebase() {
        Debug.LogError("RemoteConfig configured and ready!");
        isFirebaseInitialized = true;
        FetchDataAsync();
    }
    
    public Task FetchDataAsync() {
        Debug.LogError("Fetching data...");
        Task fetchTask = Firebase.RemoteConfig.FirebaseRemoteConfig.FetchAsync( TimeSpan.Zero);
        return fetchTask.ContinueWithOnMainThread(FetchComplete);
    }
#endif
    
    void FetchComplete(Task fetchTask)
    {
        if (fetchTask.IsCanceled)
        {
            Debug.LogError("Fetch canceled.");
        }
        else if (fetchTask.IsFaulted)
        {
            Debug.LogError("Fetch encountered an error.");
        }
        else if (fetchTask.IsCompleted)
        {
            Debug.LogError("Fetch completed successfully!");

            String json = "";   

#if SA_FIREBASE      
#if UNITY_ANDROID
            json = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("remoteDataAndroid").StringValue;
            Debug.LogError("Loaded Android data From Firebase : " + json);
#elif UNITY_IOS
            json = Firebase.RemoteConfig.FirebaseRemoteConfig.GetValue("remoteDataIos").StringValue;
            Debug.LogError("Loaded IOS data From Firebase : " + json);
#endif           
#endif           
            
                Response response = json.Equals("") ? JsonUtility.FromJson<Response>(AdsUtils.GetStreamingJson()) : JsonUtility.FromJson<Response>(json);
                
                AdsUtils.SaveFile(response);

                if (response != null)
                {
                    SetAllAdsData(response);
                }
                else
                {
                    Debug.LogError("Responce Null in RemoteConfiguration");
                }
     
            Debug.LogError("Load AppList 1 From File " + AdsUtils.LoadFile().remoteData.getAppsList()[0].toString());
            Debug.LogError("Load AppList 2 From File " + AdsUtils.LoadFile().remoteData.getAppsList2()[0].toString());
        }
 
#if SA_FIREBASE      
        var info = Firebase.RemoteConfig.FirebaseRemoteConfig.Info;
        switch (info.LastFetchStatus) {
            case Firebase.RemoteConfig.LastFetchStatus.Success:
                Firebase.RemoteConfig.FirebaseRemoteConfig.ActivateFetched();
                Debug.LogError(String.Format("Remote data loaded and ready (last fetch time {0}).",
                    info.FetchTime));
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Failure:
                switch (info.LastFetchFailureReason) {
                    case Firebase.RemoteConfig.FetchFailureReason.Error:
                        Debug.LogError("Fetch failed for unknown reason");
                        break;
                    case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                        Debug.LogError("Fetch throttled until " + info.ThrottledEndTime);
                        break;
                }
                break;
            case Firebase.RemoteConfig.LastFetchStatus.Pending:
                Debug.LogError("Latest Fetch call still pending.");
                break;
        }
#endif
    }

    private void SetAllAdsData(Response response)
    {
        
        Advertising.AutoAdLoadingMode = AutoAdLoadingMode.LoadAllDefinedPlacements;
        Debug.LogError("SetAllAdsData admobPer : "+AdsUtils.getAdmobAdPercentages()+"% : facebookPer : "+AdsUtils.getFacebookAdPercentages()+"% : unityPer : "+AdsUtils.getUnityAdPercentages()+"% ");

        EM_Settings.Advertising.AdMob.AdPercentageSetting = (Percentage) AdsUtils.getAdmobAdPercentages();
        EM_Settings.Advertising.UnityAds.AdPercentageSetting = (Percentage) AdsUtils.getUnityAdPercentages();
        EM_Settings.Advertising.AudienceNetwork.AdPercentageSetting = (Percentage) AdsUtils.getFacebookAdPercentages();

        EM_Settings.Advertising.AdMob.EnableTestMode = AdsUtils.getAdmobTestMode();
        EM_Settings.Advertising.AudienceNetwork.EnableTestMode = AdsUtils.getFbTestMode();
        EM_Settings.Advertising.UnityAds.EnableTestMode = AdsUtils.getUnityTestMode();
        
        Debug.LogError("SetAllAdsData admobTestMode : "+EM_Settings.Advertising.AdMob.EnableTestMode
                                                       +" : facebookTestMode : "+ EM_Settings.Advertising.AudienceNetwork.EnableTestMode
                                                       +" : unityTestMode : "+EM_Settings.Advertising.UnityAds.EnableTestMode);
        
    }
}
