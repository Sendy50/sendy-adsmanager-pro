﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AppsListItem
{

    public String id;
    
    public String appRating;
    
    public String appIcon;
    
    public String appName;

    public String appDescription;
    
    public String appPackageName;

    public String getId(){
        return id;
    }
   
    public String getAppIcon(){
        return appIcon;
    }
    
    public String getAppRating(){
        return appRating;
    }
  

    public String getAppName(){
        return appName;
    }

    public String getAppDescription(){
        return appDescription;
    }

    public String getAppPackageName(){
        return appPackageName;
    }
    
    public String toString(){
        return 
            "AppsListItem{" + 
            ",app_id = '" + id + '\'' + 
            ",app_name = '" + appName + '\'' + 
            ",app_icon = '" + appIcon + '\'' + 
            ",app_rating = '" + appRating + '\'' + 
            ",app_description = '" + appDescription + '\'' + 
            ",app_packageName = '" + appPackageName + '\'' + 
            "}";
    }
    
}
