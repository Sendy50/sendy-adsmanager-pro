﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RemoteData
{
    public OptionalApp optional_app;

    public Unity unity;

    public String app_availability;

    public Facebook facebook;

    public String version_code;

    public Admob admob;

    public List<AppsListItem> apps_list;

    public List<AppsListItem> apps_list2;

    public void setOptionalApp(OptionalApp optionalApp){
        this.optional_app = optionalApp;
    }

    public OptionalApp getOptionalApp(){
        return optional_app;
    }

    public void setUnity(Unity unity){
        this.unity = unity;
    }

    public Unity getUnity(){
        return unity;
    }

    public void setAppAvailability(String appAvailability){
        this.app_availability = appAvailability;
    }

    public String getAppAvailability(){
        return app_availability;
    }

    public void setFacebook(Facebook facebook){
        this.facebook = facebook;
    }

    public Facebook getFacebook(){
        return facebook;
    }

    public void setVersionCode(String versionCode){
        this.version_code = versionCode;
    }

    public String getVersionCode(){
        return version_code;
    }

    public void setAdmob(Admob admob){
        this.admob = admob;
    }

    public Admob getAdmob(){
        return admob;
    }

    public void setAppsList(List<AppsListItem> appsList){
        this.apps_list = appsList;
    }

    public List<AppsListItem> getAppsList(){
        return apps_list;
    }
    
    public List<AppsListItem> getAppsList2(){
        return apps_list2;
    }

    public String toString(){
        return 
            "RemoteData{" + 
            "optional_app = '" + optional_app + '\'' + 
            ",unity = '" + unity + '\'' + 
            ",app_availability = '" + app_availability + '\'' + 
            ",facebook = '" + facebook + '\'' + 
            ",version_code = '" + version_code + '\'' + 
            ",admob = '" + admob + '\'' + 
            ",apps_list = '" + apps_list + '\'' + 
            ",apps_list2 = '" + apps_list2 + '\'' + 
            "}";
    }
}


