﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class OptionalApp 
{
    public String app_icon;

    public String app_name;

    public String app_description;

    public String app_packageName;

    public void setAppIcon(String appIcon){
        this.app_icon = appIcon;
    }

    public String getAppIcon(){
        return app_icon;
    }

    public void setAppName(String appName){
        this.app_name = appName;
    }

    public String getAppName(){
        return app_name;
    }

    public void setAppDescription(String appDescription){
        this.app_description = appDescription;
    }

    public String getAppDescription(){
        return app_description;
    }

    public void setAppPackageName(String appPackageName){
        this.app_packageName = appPackageName;
    }

    public String getAppPackageName(){
        return app_packageName;
    }


    public String toString(){
        return 
            "OptionalApp{" + 
            "appIcon = '" + app_icon + '\'' + 
            ",appName = '" + app_name + '\'' + 
            ",appDescription = '" + app_description + '\'' + 
            ",appPackageName = '" + app_packageName + '\'' + 
            "}";
    }
}
