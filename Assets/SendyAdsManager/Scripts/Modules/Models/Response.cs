﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Response 
{
   
 
    public RemoteData remoteData;
    
    public void setRemoteData(RemoteData remoteData){
        this.remoteData = remoteData;
    }

    public RemoteData getRemoteData(){
        return remoteData;
    }

    public String toString(){
        return 
            "Response{" + 
            "remoteData = '" + remoteData + '\'' + 
            "}";
    }
}


