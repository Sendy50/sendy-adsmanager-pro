﻿using System;
using System.Collections;
using System.Collections.Generic;
using SendyAdsManager.MiniJSON;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AppItem : MonoBehaviour
{
    public Image InstallBtn;
    public Image TitleBg;
    public Image AppIcon;
    public Image AdsLable;
    public Text AppName;
    public Text StarText;
    public Text Rating;
    public Text Descriptiontext;
    public Color[] bgColor;
    private string url;
    private Color SelectedColor;


    private void Start()
    {
        // InstallBtn.onClick.AddListener(InstallBtnClick());
        SelectedColor = bgColor[0];
    }

    private string GetAppUrl(string itemAppPackageName)
    {
        return "market://details?id=" + itemAppPackageName;
        // "market://details?id=" +
        // "https://play.google.com/store/apps/details?id="
    }

    public void SetUi(AppsListItem item)
    {
        Debug.LogError("Set DAta : "+item.toString());
        AppName.text = item.getAppName();
        Rating.text = item.getAppRating();
        Descriptiontext.text = item.getAppDescription();
        url = GetAppUrl(item.getAppPackageName());
        
#if UNITY_IOS
        StartCoroutine(SetAppIcon(item.getAppIcon()));
       
#elif UNITY_ANDROID
        StartCoroutine(SetAppIcon(item.getAppIcon()));
#endif

        SelectedColor = bgColor[Random.Range(0, 9)];//(SelectedColor == bgColor[Random.Range(0, 9)])? bgColor[Random.Range(0, 9)] : SelectedColor;
        TitleBg.color = InstallBtn.transform.GetChild(0).GetComponent<Image>().color = AdsLable.color = SelectedColor;
    }
    

    IEnumerator SetAppIcon(string itemAppIcon) {
        WWW www = new WWW(itemAppIcon);
        yield return www;
        AppIcon.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));

        Color Temcolor = www.texture.GetPixel(50, 50);
        if (!((Temcolor.r + Temcolor.g + Temcolor.b) >= 0.15f))
        {
            TitleBg.color = InstallBtn.transform.GetChild(0).GetComponent<Image>().color = AdsLable.color = Temcolor;//AverageColorFromTexture(www.texture);//www.texture.GetPixel(10, 10);
        }
       
        Debug.LogError(AppName.text + "color : "+Temcolor +"Total "+(Temcolor.r + Temcolor.g + Temcolor.b) );
     
        www.Dispose();
        www = null;
    }
    
    Color32 AverageColorFromTexture(Texture2D tex)
    {
        Color32[] texColors = tex.GetPixels32();
        int total = texColors.Length;
        float r = 0;
        float g = 0;
        float b = 0;
 
        for(int i = 0; i < total; i++)
        {
            r += texColors[i].r;
            g += texColors[i].g;
            b += texColors[i].b;
        }
        return new Color32((byte)(r / total) , (byte)(g / total) , (byte)(b / total) , 0);
    }

    public void InstallBtnClick()
    {
#if UNITY_IOS
            url = "itms-apps://itunes.apple.com/app/id"+url;
#endif
        Debug.LogError("InstallClick : URL : "+url);
        Application.OpenURL(url);
    }
}
