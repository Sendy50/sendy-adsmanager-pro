﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Facebook 
{
    
    public String reward_ad;


    public String percentages;

    public String test;

    public String interstitial_ad;

    
    public String native_ad;

    
    public String banner_ad;

    public void setRewardAd(String rewardAd){
        this.reward_ad = rewardAd;
    }

    public String getRewardAd(){
        return reward_ad;
    }
    
    public bool getTestMode(){
        return test == "1";
    }

    public void setPercentages(String percentages){
        this.percentages = percentages;
    }

    public String getPercentages(){
        return percentages;
    }

    public void setInterstitialAd(String interstitialAd){
        this.interstitial_ad = interstitialAd;
    }

    public String getInterstitialAd(){
        return interstitial_ad;
    }

    public void setNativeAd(String nativeAd){
        this.native_ad = nativeAd;
    }

    public String getNativeAd(){
        return native_ad;
    }

    public void setBannerAd(String bannerAd){
        this.banner_ad = bannerAd;
    }

    public String getBannerAd(){
        return banner_ad;
    }


    public String toString(){
        return 
            "Facebook{" + 
            "reward_ad = '" + reward_ad + '\'' + 
            ",percentages = '" + percentages + '\'' + 
            ",interstitial_ad = '" + interstitial_ad + '\'' + 
            ",native_ad = '" + native_ad + '\'' + 
            ",banner_ad = '" + banner_ad + '\'' + 
            "}";
    }
}
