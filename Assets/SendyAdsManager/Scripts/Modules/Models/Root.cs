﻿using System.Collections.Generic;
using UnityEngine.Networking;

[System.Serializable]
public class Root    {
  
    public int ResultCount { get; set; } 

    public List<Result> Results { get; set; } 
}