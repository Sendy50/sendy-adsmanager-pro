﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Result    {
        public List<string> screenshotUrls { get; set; } 
        public List<object> ipadScreenshotUrls { get; set; } 
        public List<object> appletvScreenshotUrls { get; set; } 
        public string artworkUrl60 { get; set; } 
        public string artworkUrl512 { get; set; } 
        public string artworkUrl100 { get; set; } 
        public string artistViewUrl { get; set; } 
        public List<string> supportedDevices { get; set; } 
        public List<object> advisories { get; set; } 
        public bool isGameCenterEnabled { get; set; } 
        public string kind { get; set; } 
        public List<object> features { get; set; } 
        public string trackCensoredName { get; set; } 
        public List<string> languageCodesISO2A { get; set; } 
        public string fileSizeBytes { get; set; } 
        public string sellerUrl { get; set; } 
        public string contentAdvisoryRating { get; set; } 
        public int averageUserRatingForCurrentVersion { get; set; } 
        public int userRatingCountForCurrentVersion { get; set; } 
        public int averageUserRating { get; set; } 
        public string trackViewUrl { get; set; } 
        public string trackContentRating { get; set; } 
        public int trackId { get; set; } 
        public string trackName { get; set; } 
        public DateTime currentVersionReleaseDate { get; set; } 
        public string releaseNotes { get; set; } 
        public DateTime releaseDate { get; set; } 
        public List<string> genreIds { get; set; } 
        public string formattedPrice { get; set; } 
        public string primaryGenreName { get; set; } 
        public string minimumOsVersion { get; set; } 
        public bool isVppDeviceBasedLicensingEnabled { get; set; } 
        public string sellerName { get; set; } 
        public int primaryGenreId { get; set; } 
        public string currency { get; set; } 
        public string version { get; set; } 
        public string wrapperType { get; set; } 
        public int artistId { get; set; } 
        public string artistName { get; set; } 
        public List<string> genres { get; set; } 
        public double price { get; set; } 
        public string description { get; set; } 
        public string bundleId { get; set; } 
        public int userRatingCount { get; set; } 
    }
