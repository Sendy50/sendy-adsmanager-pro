﻿using System.Collections.Generic;

namespace SendyAdsManager
{
    public interface IIOSInfoItemRequired
    {
        List<iOSInfoPlistItem> GetIOSInfoPlistKeys();
    }
}
