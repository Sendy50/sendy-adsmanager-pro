﻿using System;

namespace SendyAdsManager
{
    public enum Submodule
    {
        None = -1,

        // Native APIs submodules
        Media = 0,
        Contacts,

        // Utilities submodules
        RatingRequest
    }
}
