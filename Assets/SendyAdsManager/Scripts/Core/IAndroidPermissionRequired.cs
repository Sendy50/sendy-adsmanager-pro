﻿using System.Collections.Generic;

namespace SendyAdsManager
{
    public interface IAndroidPermissionRequired
    {
        List<AndroidPermission> GetAndroidPermissions();
    }
}
