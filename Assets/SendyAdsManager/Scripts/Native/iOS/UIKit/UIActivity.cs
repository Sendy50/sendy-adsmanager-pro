﻿#if UNITY_IOS
using System;
using System.Runtime.InteropServices;
using AOT;
using SendyAdsManager.Internal;
using SendyAdsManager.Internal.iOS;
using SendyAdsManager.iOS.Foundation;

namespace SendyAdsManager.iOS.UIKit
{
    internal class UIActivity : iOSObjectProxy
    {
        public class UIActivityType : NSString
        {
            internal UIActivityType(IntPtr selfPointer) : base(selfPointer)
            {
            }
        }

        internal UIActivity(IntPtr selfPointer)
            : base(selfPointer)
        {
        }
    }
}
#endif
